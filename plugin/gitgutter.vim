" Without this - ugly gray
" https://github.com/airblade/vim-gitgutter/issues/838
highlight SignColumn ctermbg=black
highlight GitGutterAdd ctermfg=darkgreen
highlight GitGutterChange ctermfg=yellow
highlight GitGutterDelete ctermfg=red
