"b0bf3ee:ci/Jenkinsfile|240| ITC-721 Jenkinsfile: add git tag, try 5
"267c063:ci/Jenkinsfile|240| ITC-721 Jenkinsfile: add git tag, try 4 (#518)
"48683c7:ci/Jenkinsfile|240| ITC-721 Jenkinsfile: add git tag, third try (#517)
"f8013c1:ci/Jenkinsfile|241| ITC-721 Jenkinsfile add git tag, second try (#516)
"2419c8b:ci/Jenkinsfile|225| Jenkinsfile: DRY cli plan parallel (#268)
"
" With this change:
" b0bf3ee:ci/Jenkinsfile|238| 2023-03-31 || Vitaly Zdanevich     || ITC-721 Jenkinsfile: add git tag, try 5
" 267c063:ci/Jenkinsfile|238| 2023-03-31 || Vitaly Zdanevich     || ITC-721 Jenkinsfile: add git tag, try 4 (#518)
" 48683c7:ci/Jenkinsfile|238| 2023-03-31 || Vitaly Zdanevich     || ITC-721 Jenkinsfile: add git tag, third try (#517)
" f8013c1:ci/Jenkinsfile|239| 2023-03-30 || Vitaly Zdanevich     || ITC-721 Jenkinsfile add git tag, second try (#516)
" 2419c8b:ci/Jenkinsfile|223| 2022-06-16 || Vitaly Zdanevich     || Jenkinsfile: DRY cli plan parallel (#268)

" From https://github.com/tpope/vim-fugitive/issues/1603#issuecomment-1319459408
" For `:Gclog` in order to show datetime, author.
" Without (the default is "%s"):
let g:fugitive_summary_format = "%cs || %<(20,trunc)%an || %s"
