let g:fzf_preview_window = ['up,50%', 'ctrl-/']

let g:fzf_history_dir = '~/.local/share/fzf-history'
" With this you can Ctrl-N/Ctrl-P to get the history of searches
" https://github.com/junegunn/fzf.vim/blob/e198586037a879790df6ca3245d4f61583cb9dc9/doc/fzf-vim.txt#L153-L157

nnoremap <leader>f :call fzf#vim#files(expand('%:h'), {'options': ['--preview', 'cat {}', '--preview-window=right,70%']}, 1)<CR>
" Grep by filenames, in the folder of the current file.
" I have this also because of Neovim - fzf_preview_window looks like is for Vim only,
" but I want to have fzf in fullscreen.
" see https://github.com/junegunn/fzf.vim/issues/1471
" In order to search for files not only in the current folder use :Files
" see https://github.com/junegunn/fzf.vim#commands

nnoremap <leader>e :call SearchWithIgnore()<CR>
" Exclude tests, mocks

function! SearchWithIgnore()
    let c = 'rg
        \ --glob "!*_test.go"
        \ --glob "!*_mock.go"
        \ --glob "!*.gen.go"
        \ --glob "!mock/"
        \ --glob "!mocks/"
        \ --line-number
        \ --no-heading
        \ --color=always
        \ --smart-case
        \ --fixed-strings
        \ -- %s || true'
    let spec = {'options': ['--disabled', '--bind', 'change:reload:'.printf(c, '{q}')]}
    let spec = fzf#vim#with_preview(spec, 'up', 'ctrl-/')
    call fzf#vim#grep(c, 1, spec, 1)
endfunction

nnoremap <leader>t :call SearcTests()<CR>

function! SearcTests()
    let c = 'rg
        \ --glob "*_test.go"
        \ --glob "!*_mock.go"
        \ --glob "!*.gen.go"
        \ --glob "!mock/"
        \ --glob "!mocks/"
        \ --line-number
        \ --no-heading
        \ --color=always
        \ --smart-case
        \ --fixed-strings
        \ -- %s || true'
    let spec = {'options': ['--disabled', '--bind', 'change:reload:'.printf(c, '{q}')]}
    let spec = fzf#vim#with_preview(spec, 'up', 'ctrl-/')
    call fzf#vim#grep(c, 1, spec, 1)
endfunction

nnoremap <silent> <Leader>ag :Ag <C-R><C-W><CR>
xnoremap <silent> <Leader>ag y:Ag <C-R>"<CR>
" Search for words under the cursor
" https://github.com/junegunn/fzf.vim/issues/1474#issuecomment-1576046719
" TODO currently does not works for Go files
