nnoremap <silent> <leader>h :call CocActionAsync('definitionHover')<cr>

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-@> coc#refresh()

" Increase context size on GR
call coc#config('list', { 'height': 50, 'maxPreviewHeight': 50, 'previewToplineOffset': 25 })

" Floating window for definitionHover (when cursor is on an error - without hotkeys)
" by default is too small for many errors,
" see https://github.com/neoclide/coc.nvim/discussions/3943
" https://github.com/neoclide/coc.nvim/blob/2e3528869fa4e0c6292c429c99bf3199bf0619a6/doc/coc-config.txt#L818
" https://github.com/neoclide/coc.nvim/blob/2e3528869fa4e0c6292c429c99bf3199bf0619a6/doc/coc-config.txt#L317
call coc#config('diagnostic', { 'floatConfig': {'pumpheight': 20, 'maxWidth': 300 }})

" https://github.com/neoclide/coc.nvim/discussions/4259#discussioncomment-3823638
call coc#config('hover', { 'floatConfig': {'maxWidth': 130, 'border': 'true' }})
call coc#config('floatFactory', { 'floatConfig': {'maxWidth': 130 }})
call coc#config('outline', { 'floatConfig': {'previewMaxWidth': 130 }})

call coc#config('notification', { 'minProgressWidth': 200, 'maxWidth': 200 })
" Without this - width is not enough to see the error message, see
" https://github.com/neoclide/coc.nvim/issues/4878#issuecomment-1922611652

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: There's always complete item selected by default, you may want to enable
" no select by `"suggest.noselect": true` in your configuration file.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

nmap <silent><leader>t <Plug>(coc-diagnostic-info)

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)


" Run the Code Lens action on the current line.
nmap <leader>cl  <Plug>(coc-codelens-action)

" Make <CR> to accept selected completion item or notify coc.nvim to format
" <C-g>u breaks current undo, please make your own choice.
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)


