let b:ale_linters = {'python': ['pylint', 'flake8', 'mypy']}

let g:ale_echo_msg_format = '[%linter%] %[code]% %s [%severity%]'
let g:ale_lint_on_enter = 0  " Against slow switching between buffers
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
" https://github.com/w0rp/ale#5ix-how-can-i-navigate-between-errors-quickly
