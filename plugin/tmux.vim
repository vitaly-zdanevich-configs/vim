if exists('$TMUX')
        " tmux: show title of the document in tabs"
        autocmd BufReadPost,FileReadPost,BufNewFile * call system("tmux rename-window " . expand("%:t"))
        autocmd VimLeave * call system("tmux setw automatic-rename")
        " https://stackoverflow.com/questions/15123477/tmux-tabs-with-name-of-file-open-in-vim
endif

