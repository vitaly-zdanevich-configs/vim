" This file is for Neovim

set clipboard=unnamedplus
" System clipboard will be available after Nvim exit or Ctrl-Z.
" https://stackoverflow.com/a/64794357/1879101

source /home/vitaly/.config/vim/vimrc
source /home/vitaly/.config/vim/firenvim.vim
