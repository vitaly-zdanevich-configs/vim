autocmd UIEnter * set lines=100
autocmd UIEnter * set columns=200
" firenvim: for fullscreen https://github.com/glacambre/firenvim/issues/426

set guifont=:h10

let g:firenvim_config = {
	\ 'globalSettings': {
		\ 'alt': 'all',
	\  },
	\ 'localSettings': {
		\ '.*': {
			\ 'cmdline': 'neovim',
			\ 'priority': 0,
			\ 'selector': 'textarea',
			\ 'takeover': 'never',
		\ },
	\ }
\ }

inoremap <C-S-v> <C-r>+
" Enable Ctrl-Shift-v (paste) in insert mode
" https://github.com/glacambre/firenvim/issues/1182#issuecomment-2580734776
