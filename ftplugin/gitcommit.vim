" https://stackoverflow.com/questions/16727923/how-to-modify-the-last-position-jump-vimscript-to-not-do-it-for-git-commit-messa
call setpos('.', [0, 1, 1, 0])

" https://stackoverflow.com/questions/1691060/vim-set-spell-in-file-git-commit-editmsg
setlocal wrap linebreak spell
