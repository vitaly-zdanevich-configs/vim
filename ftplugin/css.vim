" This enable autocompletion (Ctrl + X P) for strings like aaa-bbb-ccc,
" without that will suggest only aaa for a.
" https://stackoverflow.com/questions/10789430/vims-ctrlp-autocomplete-for-identifiers-with-dash
setlocal iskeyword+=\-
