" activate build-in plugin:
" use % to go from opening xml tag to closing
" and vice versa
" - like moving from { to } in default preferences
runtime macros/matchit.vim

" go to opening or closing of parent tag
nnoremap ]t vatatv
nnoremap [t vatatov
" https://stackoverflow.com/questions/6169537/vim-movements-going-to-parent
