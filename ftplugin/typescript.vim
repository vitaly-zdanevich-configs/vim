" Tab size
set expandtab ts=2

" Shift width - size of manual mass indent by > or < on selected
set sw=2

let g:coc_global_extensions = ['coc-tsserver']

call coc#config('typescript', {
	\ 'format':  {'insertSpaceAfterOpeningAndBeforeClosingNonemptyBraces': 'true'},
	\ 'preferences': {'quoteStyle': 'single'}
\ })
