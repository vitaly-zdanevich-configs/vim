let g:ale_python_flake8_options = '--max-line-length=100'

" https://github.com/vim-python/python-syntax#configuration
let g:python_highlight_all = 1
" This enables all highlights from improved python highlighter plugin
" I need it for template literals
