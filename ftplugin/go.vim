set noexpandtab
set copyindent
set preserveindent
set softtabstop=0
set shiftwidth=4
set tabstop=4
" From https://vim.fandom.com/wiki/Indent_with_tabs,_align_with_spaces

" === vim-go ===
let g:go_gopls_local='github.com/River-Island'

if !has('nvim')
    setl balloonevalterm
    setl balloonexpr=go#tool#DescribeBalloon()
endif

" let g:go_debug = ['shell-commands', 'lsp']
" Uncomment this in order to see debug messages

" === /vim-go ===
