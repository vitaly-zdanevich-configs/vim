syntax on

" Set the title for the current terminal tab: folder name and relative path, like:
" cart/src/utils/index.ts 9
" with line number at the end
set title titlestring=%m%{fnamemodify(getcwd(),':t')}/%f%=%l

" Do not create .swp.* files
" against committing this .swp, adding them to archive, grep, ...
" also for faster Vim.
set noswapfile
" https://stackoverflow.com/a/821936/1879101

set tabstop=4
"tab rendered size (view only)

set shiftwidth=4
" shift width - size of manual mass indent by > or < on selected

set backspace=indent,eol,start
" With this backspace works and I can remove to the upper lines too
" https://stackoverflow.com/questions/11560201/backspace-key-not-working-in-vim-vi

set listchars=eol:$,tab:>-,extends:>,precedes:< " Show non-printable characters
set list " Shows tabs and end of line
" http://vim.wikia.com/wiki/Highlight_unwanted_spaces
" for another non-keyboard chars https://unicode-table.com
" but not all terms can display all of unicode,
" also over ssh you can see not all chars

set cursorline " underline line with cursor

set cursorcolumn
augroup BgHighlight
    autocmd!
    autocmd WinEnter * set cursorcolumn
    autocmd WinLeave * set nocursorcolumn
augroup END
" https://superuser.com/questions/385553/making-the-active-window-in-vim-more-obvious

set nostartofline
" On buffer switch: remember cursor position, on the line.
" Solution from https://stackoverflow.com/a/62820338/1879101

" incremental search - search on every char
set incsearch

" Search count found at the bottom right
" https://stackoverflow.com/a/60840571/1879101
set shortmess-=S

set wildmenu
":e xxx<tab> show variants
" See https://vi.stackexchange.com/questions/11872/how-do-i-use-wildmenu-effectively

set laststatus=0 " Do not show statusline
highlight StatusLine ctermbg=black ctermfg=brown  " Windows splitted by status line
highlight StatusLineNC ctermbg=black ctermfg=darkgray " NC mean Not Current

source $HOME/.config/vim/highlights.vim

" search selected by //
" http://vim.wikia.com/wiki/Search_for_visually_selected_text
vnoremap // y/<C-R>"<CR>

" highlight search
set hlsearch
"
set updatetime=100
" For vim-gitgutter - to increase speed of diff signs update, default is 4000.
" See https://github.com/airblade/vim-gitgutter/blob/master/README.mkd#getting-started

" Switch to last-active tab.
" From
" https://stackoverflow.com/questions/2119754/switch-to-last-active-tab-in-vim
" Use
" \`
" \ is a default <leader>
if !exists('g:Lasttab')
    let g:Lasttab = 1
    let g:Lasttab_backup = 1
endif
autocmd! TabLeave * let g:Lasttab_backup = g:Lasttab | let g:Lasttab = tabpagenr()
autocmd! TabClosed * let g:Lasttab = g:Lasttab_backup
nmap <silent> <Leader>` :exe "tabn " . g:Lasttab<cr>

" increase limit of copied (yanked) lines:
" '100 marks will be remembered for the last 100 edited files;
" (this can not be deleted though I do not need it)
" maximum 1000 lines instead of default 50;
" maximum 20 kilobytes;
" http://vim.wikia.com/wiki/Copy,_cut_and_paste
set viminfo='100,<1000,s20

" enable mouse click to move caret and select with mouse
set mouse=a
" https://superuser.com/questions/146768/mouse-cursor-in-terminal

" vim-signature: sync color with git-gutter 
let g:SignatureMarkTextHLDynamic = 1

" Get current color name
" How to use:
" :call ShowColourSchemeName()
" https://stackoverflow.com/a/2419692/1879101
function! ShowColourSchemeName()
    if exists('g:colors_name')
        echo g:colors_name
    else
        echo "default"
    endif
endfunction

" Context ("unfilied" in Git terminology) of git difftool
set diffopt=filler,context:9

set splitkeep=topline
" Stabilize window open/close: do not scroll main window, when you open, for
" example, help, or quicklist, when resize.
" Why I have this:
" 1) To not loose the focus
" 2) Vim scrolling performance is bad - so do not scroll when you want
" This does not affect :version
" This is default behavior in Neovim

" Against very slow Vim when big oneline.
" https://stackoverflow.com/questions/901313
set nowrap

" print the name of the current function in command line by Ctrl + g f
nnoremap <C-g>f :echo cfi#format("%s", "")<CR>

" Scroll down/up with cursor in the middle of the screen
" I got this idea from https://youtu.be/KfENDDEpCsI?list=PLm323Lc7iSW_wuxqmKx_xxNtJC_hJbQ7R&t=220
nnoremap <C-d> <C-d>zz
nnoremap <C-u> <C-u>zz

" In command line at right - show the amount of selected chars or lines (in Visual mode)
set showcmd

" Bottom right: do not show vertical/horizontal position numbers
set noruler

set encoding=utf-8

" Remain undo/redo when switching between buffers
set hidden

" History of commands (:) and search (/); you see history at :his or q:, good for completion
set history=2000

" activate setting of prefs per file in comments
set modeline
" http://stackoverflow.com/questions/313463

" Read .vimrc from current folder
set exrc
set secure

" Mapping to move lines
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv
" https://vim.fandom.com/wiki/Moving_lines_up_or_down#Mappings_to_move_lines

command Copy call system('xsel --clipboard --input', @") | echo 'Copied'
" Copy unnamed ("default") register to the system clipboard.
" I have this command because `apt install vim` on Ubuntu
" install version with '-clipboard', without access to the system clipboard.
" I need this command to copy selected -
" I tried to send selected to external command but Vim send entire line, see
" https://stackoverflow.com/a/40073358/1879101

command! Path let @+ = expand('%:p') | echo 'Absolute path copied to the main system clipoard'
" From https://vi.stackexchange.com/a/3687/6038
" As usual - Vim must be active in order to paste,
" because in X11 "clipoard" mean - the app will send some data on paste (Ctrl-V).
"
"
command! P let @+ = expand('%') | echo 'Relative path copied to the main system clipoard'
" TODO extract Path and P to my first Vim plugin? In order to decompose. Check
" if such plugin already exists

" Bottom is before a few lines
set scrolloff=2

set backupdir=$HOME/.config/vim/tmp/backup
" Against such files in $HOME: .viminfd.tmp    .viminff.tmp    .viminfh.tmp
" From https://stackoverflow.com/a/61585014/1879101
