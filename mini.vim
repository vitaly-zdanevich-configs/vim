" This is a minimal vimrc.
" When I have some problem with Vim -
" I can use it with
" vim -u ~/.config/vim/mini.vim
" You can add plugins and configs one by one

set nocompatible
set runtimepath^=/home/vitaly/.vim/pack/git-submodules-plugins/start/coc.nvim
filetype plugin indent on
syntax on
set hidden
