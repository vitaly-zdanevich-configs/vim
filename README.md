* Clone with `--recursive` - for getting all submodules (plugins in `/pack`)

# To update all submodules to latest commit in master:
git submodule update --recursive --remote

# To update one submodule:
cd to directory and `git pull`

# For linting of API Blueprint - install parser [drafter](https://github.com/apiaryio/drafter) - needed Syntastic already in /bundle
brew install --HEAD https://raw.github.com/apiaryio/drafter/master/tools/homebrew/drafter.rb

# Enable Go linting
```
cd ~/.config/vim/pack/lsp/start/coc
npm ci
```

In Vim:
```
:CocInstall coc-go
:CocList extensions
```

More info: https://github.com/neoclide/coc.nvim/wiki/Install-coc.nvim

# For validation of js by Syntastic:
`npm install -g eshint`
https://www.sitepoint.com/comparison-javascript-linting-tools/

# Install tern:
```bash
cd bundle/tern_for_vim
npm install
```

# For validation html by the Syntastic:
`brew install tidy-html5`

# For Python3 for Syntastic:
```bash
pip3 install pylink, flake8, mypy
```
If permission denied on install: add --user

# How to add new plugin:  
```bash
cd pack/git-submodules-plugins/start
git submodule add http://github.com/davidhalter/jedi-vim.git  
```
Possible to execute for plugin that already exists in bundle/

# To remove submodule/plugin:
git rm <name>
