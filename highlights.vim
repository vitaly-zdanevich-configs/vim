" To get current value:
" :hi CocHighlightText
"
" See all color values at
" https://vim.fandom.com/wiki/Xterm256_color_names_for_console_Vim

if system('uname') == "Darwin\n"
	" `if has('mac')` wors only for Vim from Homebrew - not for preinstalled.

	" Theme https://github.com/tomasr/molokai used not as a candy but because
	" default theme with js syntax plugin highlight in the same color strings and
	" the names of reserved variables like `console` or `Number`, see my issue at
	" https://github.com/pangloss/vim-javascript/issues/1019
	" Also this theme alter horizontal highlighter of the current line
	" from underline to background to the full height of the line.
	" colorscheme molokai" in directory `colors`
	"let g:molokai_original = 1
	 " runtime colors/gotham/colors/gotham256.vim " better for default term app in macOS when Molokai is better for iTerm2
	colorscheme zaibatsu

	highlight CursorColumn ctermbg=17
	" looks like almost transparent darkblue
	" this is darker visible that can be
	" check for all possible colors here
	" https://github.com/guns/xterm-color-table.vim
	" TODO copy code from link to this file
		" for checking colors on different envs -
		" because I found that for example the same color value
		" looks different on tty and in xterm, also tty
		" have less colors
else
	if &term == 'linux'
		" tty on Ctrl+Alt+F1,
		" Terminator on X returns "xterm"
		highlight CursorColumn ctermbg=5
		highlight CursorLine ctermbg=red
	else  " X terminals like Gnome Terminator, Alacritty, kitty

		" Color of the delimiter of two opened windows
		" ctermbg setted to have invisible ascii in xterm
		highlight VertSplit ctermbg=black ctermfg=black

		highlight CurrentWord cterm=bold
		highlight CurrentWordTwins ctermfg=black ctermbg=darkgreen

		highlight TabLineFill ctermfg=235
		" Tab background

		highlight TabLine cterm=italic ctermfg=245 ctermbg=234
		" Tab inactive, https://stackoverflow.com/questions/7238113/customising-the-colours-of-vims-tab-bar

		highlight CursorLine   cterm=NONE ctermbg=17	   ctermfg=white
		highlight CursorColumn			ctermbg=235
		highlight Normal				  ctermbg=None
		highlight Search				  ctermbg=magenta  ctermfg=black
		highlight Visual								   ctermfg=black

		highlight SpellBad				ctermbg=red
		highlight SpellBad								 ctermfg=black
		highlight SpellCap								 ctermfg=black

		highlight Pmenu				   ctermbg=232	  ctermfg=gray

		highlight PmenuSel				ctermbg=darkgray ctermfg=white
		highlight CocMenuSel			  cterm=bold	   ctermbg=17

		highlight PmenuSbar			   ctermbg=black
		highlight PmenuThumb			  ctermbg=darkgray

		" coc-references color in location list
		highlight CocListLine cterm=bold ctermbg=2 ctermfg=black

	endif
endif
